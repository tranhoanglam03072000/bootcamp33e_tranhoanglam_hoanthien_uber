const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
function tinhGiaTienKMDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }
  if (car == UBER_SUV) {
    return 9000;
  }
  if (car == UBER_BLACK) {
    return 11000;
  }
}
function tinhGiaTienKm1_19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
    }
    case UBER_SUV: {
      return 8500;
    }
    case UBER_BLACK: {
      return 9500;
    }
    default:
      return 0;
  }
}
function tinhGiaTienKmTren19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
    default:
      return 0;
  }
}
document.getElementById("tinhTienUber").onclick = function () {
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;
  console.log("carOption: ", carOption);
  var giaTienKmDauTien = tinhGiaTienKMDauTien(carOption);
  console.log("giaTienKmDauTien: ", giaTienKmDauTien);
  var giaTienKm1_19 = tinhGiaTienKm1_19(carOption);
  console.log("giaTienKm1_19: ", giaTienKm1_19);
  var giaTienKmTren19 = tinhGiaTienKmTren19(carOption);
  console.log("giaTienKmTren19: ", giaTienKmTren19);

  var nhapKM = document.getElementById("txt-km").value * 1;
  //output:
  var xuatTien = 0;

  if (nhapKM <= 1) {
    xuatTien = giaTienKmDauTien * nhapKM;
  } else if (nhapKM <= 19) {
    xuatTien = giaTienKmDauTien + (nhapKM - 1) * giaTienKm1_19;
  } else if (nhapKM > 19) {
    xuatTien =
      giaTienKmDauTien + giaTienKm1_19 * 18 + (nhapKM - 19) * giaTienKmTren19;
  }
  document.getElementById("xuattien").innerHTML =
    xuatTien.toLocaleString() + " VND";
};
